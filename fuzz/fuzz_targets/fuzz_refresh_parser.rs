#![no_main]

use libfuzzer_sys::fuzz_target;

use refresh_parser::parse_refresh_content;

fuzz_target!(|content: String| {
    let _ = parse_refresh_content(content);
});
