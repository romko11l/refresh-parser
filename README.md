# refresh_parser

Modern browsers have several redirect mechanisms. One of them is `refresh`. You can use it in:

- HTTP header
```
Refresh: 5; url=https://gitlab.com/
```

- HTML content
```html
<meta http-equiv="Refresh" content="5; URL=https://gitlab.com/" />
```

Browsers allow different ways to fill `refresh` content. Therefore, the parsing algorithms for it are rather complicated:

- https://searchfox.org/mozilla-central/rev/4d6a262deed98ddb4d609de94048e7f97b6e85d3/docshell/base/nsDocShell.cpp#5377
- https://github.com/yoavweiss/Blink/blob/0e22e374087693ab579b336e0a639e13f637fed1/Source/platform/network/HTTPParsers.cpp#L171

This repository presents the implementation of this [algorithm](https://html.spec.whatwg.org/#will-declaratively-refresh) on rust.

---

Run tests:

`cargo test`

Run fuzzing (https://rust-fuzz.github.io/book/introduction.html):

`cargo fuzz run fuzz_refresh_parser`